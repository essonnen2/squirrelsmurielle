
<!-- README.md is generated from README.Rmd. Please edit that file -->

# squirrelsmurielle

<!-- badges: start -->
<!-- badges: end -->

The goal of squirrelsmurielle is to study the squirrels

## Installation

You can install the development version of squirrelsmurielle like so:

``` r
# FILL THIS IN! HOW CAN PEOPLE INSTALL YOUR DEV PACKAGE?
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(squirrelsmurielle)
## basic example code
```

``` r
get_message_fur_color(primary_fur_color = "Cinnamon")
#> We will focus on Cinnamon squirrels
```
